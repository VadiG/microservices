export class DialogDto {
  dialog_id: number;
  author_id: number;
  text_msg: string;
}
