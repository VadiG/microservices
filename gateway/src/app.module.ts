import { Module } from '@nestjs/common';
import { AppController } from './app.controller';
import { ClientsModule, Transport } from '@nestjs/microservices';

@Module({
  imports: [
    ClientsModule.register([
      {
        name: 'DIALOG_MICROSERVICE',
        transport: Transport.TCP,
        options: {
          host: 'dialog',
          port: 8000,
        },
      },
      {
        name: 'USER_MICROSERVICE',
        transport: Transport.TCP,
        options: {
          host: 'user',
          port: 8000,
        },
      },
    ]),
  ],
  controllers: [AppController],
  providers: [],
})
export class AppModule {}
