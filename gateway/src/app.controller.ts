import {
  Body,
  Controller,
  Get,
  Inject,
  Param,
  ParseIntPipe,
  Post,
} from '@nestjs/common';
import { ClientProxy } from '@nestjs/microservices';
import { DialogDto } from './dto/dialog.dto';
import { Observable } from 'rxjs';
import { UserDto } from './dto/user.dto';

@Controller()
export class AppController {
  constructor(
    @Inject('DIALOG_MICROSERVICE') private dialogClient: ClientProxy,
    @Inject('USER_MICROSERVICE') private userClient: ClientProxy,
  ) {}

  @Post('/dialog')
  createDialog(@Body() dialog: DialogDto): Observable<any> {
    return this.dialogClient.send({ role: 'dialog', cmd: 'add' }, dialog);
  }

  @Get('/dialog/:id')
  getDialog(@Param('id', ParseIntPipe) id: number): Observable<any> {
    return this.dialogClient.send(
      { role: 'dialog', cmd: 'get-by-user-id' },
      id,
    );
  }

  @Get('/user')
  getUser() {
    return this.userClient.send({ role: 'user', cmd: 'get-all' }, {});
  }

  @Post('/user')
  createUser(@Body() user: UserDto): Observable<any> {
    return this.dialogClient.send({ role: 'user', cmd: 'add' }, user);
  }
}
