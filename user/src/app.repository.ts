import { Injectable } from '@nestjs/common';
import { DeliveryBuilder, User } from './entity/user';
import { UserDto } from './entity/user.dto';

@Injectable()
export class AppRepository {
  private readonly users: User[];

  constructor() {
    this.users = [];
  }

  find(): User[] {
    return this.users;
  }

  findById(id: number): User {
    return this.users.find((item) => item.id === id);
  }

  add(element: UserDto): { id: number } {
    const delivery = new DeliveryBuilder()
      .setId(this.users.length + 1)
      .setFirstName(element.firstName)
      .setSecondName(element.secondName)
      .setBirthday(element.birthday)
      .build();
    this.users.unshift(delivery);
    return { id: delivery.id };
  }
}
