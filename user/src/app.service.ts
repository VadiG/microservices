import { Injectable } from '@nestjs/common';
import { UserDto } from './entity/user.dto';
import { AppRepository } from './app.repository';
import { User } from './entity/user';

@Injectable()
export class AppService {
  constructor(private repository: AppRepository) {}

  async add(element: UserDto): Promise<{ id: number }> {
    console.log('User::add');
    return this.repository.add(element);
  }

  async getUsers(): Promise<User[]> {
    console.log('User::getUsers');
    return this.repository.find();
  }
}
