import { Controller, Get } from '@nestjs/common';
import { AppService } from './app.service';
import { MessagePattern } from '@nestjs/microservices';
import { UserDto } from './entity/user.dto';
import { User } from './entity/user';

@Controller()
export class AppController {
  constructor(private readonly appService: AppService) {}

  @MessagePattern({ role: 'user', cmd: 'add' })
  async add(element: UserDto): Promise<{ id: number }> {
    return this.appService.add(element);
  }

  @MessagePattern({ role: 'user', cmd: 'get-all' })
  async getUsers(): Promise<User[]> {
    return this.appService.getUsers();
  }
}
