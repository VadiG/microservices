export class User {
  constructor(order: User) {
    Object.assign(this, order);
  }

  id: number;
  firstName: string;
  secondName: string;
  birthday: Date;
}

export class DeliveryBuilder implements Partial<User> {
  id?: number;
  firstName?: string;
  secondName?: string;
  birthday?: Date;

  setId(id: number): this & Pick<User, 'id'> {
    return Object.assign(this, { id: id });
  }

  setFirstName(firstName: string): this & Pick<User, 'firstName'> {
    return Object.assign(this, { firstName: firstName });
  }

  setSecondName(secondName: string): this & Pick<User, 'secondName'> {
    return Object.assign(this, { secondName: secondName });
  }

  setBirthday(birthday: string): this & Pick<User, 'birthday'> {
    // 2014-04-03
    return Object.assign(this, { birthday: new Date(birthday) });
  }

  build(this: User) {
    return new User(this);
  }
}
