import { Controller } from "@nestjs/common";
import { DialogService } from './dialog.service';
import { CreateDialogDto } from './dto/create_dialog_dto';
import { MessagePattern } from "@nestjs/microservices";

@Controller()
export class DialogController {
  constructor(private readonly dialogService: DialogService) {}

  @MessagePattern({ role: 'dialogs', cmd: 'add' })
  create(createDialogDto: CreateDialogDto) {
    return this.dialogService.create(createDialogDto);
  }

  @MessagePattern({ role: 'dialogs', cmd: 'get-by-user-id' })
  getMassagesByUser(id: number) {
    return this.dialogService.getMassagesByUser(id);
  }
}
