export class CreateDialogDto {
  dialog_id: number;
  author_id: number;
  text_msg: string;
}
