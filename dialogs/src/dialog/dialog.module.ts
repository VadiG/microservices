import { Module } from '@nestjs/common';
import { DialogController } from './dialog.controller';
import { DialogService } from './dialog.service';
import { ConfigModule } from '@nestjs/config';
import { connectionFactory } from '../helpers/connection';

@Module({
  imports: [ConfigModule.forRoot()],
  controllers: [DialogController],
  providers: [DialogService, connectionFactory],
})
export class DialogModule {}
