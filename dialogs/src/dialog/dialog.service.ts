import { Inject, Injectable } from '@nestjs/common';
import { CreateDialogDto } from './dto/create_dialog_dto';
import { Client } from 'pg';

@Injectable()
export class DialogService {
  @Inject('CONNECTION')
  private readonly client: Client;

  async create(createDialogDto: CreateDialogDto) {
    const { rows } = await this.client.query(
      `INSERT INTO messages (dialog_id, author_id, text_msg) VALUES ($1, $2, $3) RETURNING *`,
      [
        createDialogDto.dialog_id,
        createDialogDto.author_id,
        createDialogDto.text_msg,
      ],
    );
    return rows[0];
  }

  async getMassagesByUser(id: number) {
    const { rows } = await this.client.query(
      `SELECT * FROM messages WHERE author_id = $1`,
      [id],
    );
    return rows;
  }
}
