import { Module } from '@nestjs/common';
import { DialogModule } from './dialog/dialog.module';

@Module({
  imports: [DialogModule]
})
export class AppModule {}
