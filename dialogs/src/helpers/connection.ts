import { ConfigModule, ConfigService } from '@nestjs/config';
import { Client } from 'pg';

export const connectionFactory = {
  provide: 'CONNECTION',
  imports: [ConfigModule],
  inject: [ConfigService],
  useFactory: async (configService: ConfigService) => {
    const client = new Client({
      user: configService.get('POSTGRES_USER'),
      host: configService.get('POSTGRES_HOST'),
      database: configService.get('POSTGRES_DATABASE'),
      password: configService.get('POSTGRES_PASS'),
      port: configService.get('POSTGRES_PORT'),
    });
    await client.connect();
    return client;
  },
};
