CREATE TABLE IF NOT EXISTS dialogs (
	id SERIAL PRIMARY KEY,
	user_id INTEGER,
    friend_id INTEGER
);

CREATE TABLE IF NOT EXISTS messages (
	id SERIAL,
	dialog_id INTEGER,
    author_id INTEGER,
    text_msg TEXT,
    CONSTRAINT fk_dialog_id
      FOREIGN KEY (dialog_id)
      REFERENCES dialogs (id)
);